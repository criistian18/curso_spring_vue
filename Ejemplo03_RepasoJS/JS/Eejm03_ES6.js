const PI = 3.1415926;
//funciones flecha = funciones lamda

let unaVar = 20;
let unTexto = "Que pasa listo";

document.write(`<br> 
texto en varias lineas
y ademas podemos mostrar
variables asi: ${unaVar} y otro texti: ${unTexto}`);
document.write("<br>"); //html es lo que le importa!

//ejemploi funcion de suma
//funciones lambda : funcione anoinimas
var suma = (x, y) => {
    return x + y;
}
document.write("<br>" + suma(3, 2)); //html es lo que le importa!

//tmb sin llaves
var suma = (x, y) => x + y;
document.write("<br>" + suma(3, 2));

//sin parentesis cuando es un unico parametro
var alcuadrado = x => x ** 2;
document.write("<br>" + alcuadrado(5));

class Dato {
    constructor(x, y = 20) {
        this.x = x;
        this.y = y;
    }
    mostrar() {
        document.write(`<br> Dato: x =${this.x} y =${this.y}`);
    }
}

class Info extends Dato {
    constructor(x, y = 20, z = 20) {
        super(x, y); //invocar al constructor del padre
        this.z = z;
    }
    mostrar() {
        super.mostrar();
        document.write(`z =${this.z}`);
    }
}
let dato = new Dato("lo que querais", 50);
dato.mostrar();
let info = new Info("otra info"); // hereda el constructor guarda espacio en memoria (x,y)
info.mostrar();