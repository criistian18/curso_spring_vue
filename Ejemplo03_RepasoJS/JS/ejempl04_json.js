//JSON: JavaScripyt Object Notation
// Otra forma de crear objetos con la notación JSON
//abrir y cerrar llaves es crear un objeto
//let formadepago = {}; //new Object() es lo mismo

let formadepago = {
    "modo": "Tarjeta credito",
    "comision:": 2,
    "activa": true,
    "preparacion": null,
    "clientes": ["Santander", "Sabadell", "BBVA", [1, 23, 55]],
};
//Leer el disco el JSON y DES-SERIALIZARLO
//null funcion convierte el parseo 3: es para que indique aumenta los espacios 
let formaPago = JSON.parse(window.localStorage.getItem("datos-forma-pago"));
let arrayVacio = [];
let datos = ["Churro", "Merinas"];
let matriz = [
    [4, 6, 5],
    [6, 7, 8]
];

formadepago.sevidor = "http://google.es"; //DUDA   Stringgify convierte un objeto es una cadena string
document.write(`<br> ${formadepago.modo} -${formadepago.clientes[1]} - ${formadepago.clientes[1]} -${matriz[1][1]}
<h2>${JSON.stringify(formadepago)}</h2> 
Usando forma HashMap: ${ formadepago["Servidor"]} 
`);
//proporicona a cada pag web un espacio en disco
//guardar en el disco duro
window.localStorage.setItem("datos-forma-pago", JSON.stringify(formadepago, null, 3));
//Stringify Convertir un objeto o structura en memoria en un formato transmitible( o enviar por red o para guarda en fichero), es SERIALIZAR, y el 
//formato puede ser texto(XML , JSON, YAML, o uni propio), binario o encriptado
//DUDA parsear es la forma coloquial de decir "leer un texto o interpretar un texto"
// converti un texto en otro texto o en este caso convertir un texto en un objeto. Cuando hablamos del ultimo caso de definicion tecnica
// es "DES-SERIALIZAR"
let frutas = `[
    { "nombre" : "pera", "precio": 20   },
    { "nombre" : "kiwi", "precio": 27   },
    { "nombre" : "fresa", "precio": 30   } ]`;

let objFrutas = JSON.parse(frutas); // DUDA
document.write(`<br> ${objFrutas[1].nombre}  -    ${objFrutas[2]["precio"]}`);