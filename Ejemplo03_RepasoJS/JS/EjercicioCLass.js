//creamos class
class Ejemplo {
    constructor(x, y = "Hola Soy el CONSTRUCTOR") {
        this.x = x;
        this.y = y;
    }

    mosEjem() {
        document.write(`<br> Instruccion x :  ${this.x} 
                        Instruccion y: ${this.y}`);
    }
}

class EjemploHijo extends Ejemplo {
    constructor(x, y = "Hola soy el CONSTRUCTOR 2", z = "Soy la letra Z") {
        super(x, y); //copia? guarda? en memoria la referencia ( parametrización )?
        this.z = z;
    }
    mosEjem() {
        super.mosEjem(); //Duda  de mañana
        document.write(` Intruccion z: ${this.z}`);
    }


}

class EjemploBebe extends EjemploHijo {
    constructor(x, y = "Hola soy el CONSTRUCTOR 3", z, a = "Soy la letra A") { //DUDA
        super(x, y, z);
        this.a = a;

    }
    mosEjem() {
        super.mosEjem(); //Duda  de mañana
        document.write(` Instruccion a: ${this.a}`);
    }
}
let ejemplo = new Ejemplo("Soy el CONSTRUCTOR Ejemplo");
ejemplo.mosEjem();
let ejemploHijo = new EjemploHijo("Soy el Constructor EjemploHijo");
ejemploHijo.mosEjem();
let ejemploBebe = new EjemploBebe("Soy el CONSUTRCTOR EjemploBebe");
ejemploBebe.mosEjem();