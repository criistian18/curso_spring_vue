Vue.component("app-saludo", {
    "template": `<h2>Hola soy un componente</h2>`
});
Vue.component("app-adios", {
    "template": `<div><h2>Adios Componente</h2>
    <h2 class="titulo">Pues eso hasta luego mari carmen!</h2>
    </div>`,
    style: `.titulo {background-color: red}`
});

new Vue({
    "el": "#app-section", //"el" : Element
    template: `
    <div><h2>Hola ICA</h2>
    <app-saludo></app-saludo>
    <app-adios></app-adios></div>`


});
new Vue({
    el: "#app-section-2"
});

new Vue({
    el: "#app-section-3"
});