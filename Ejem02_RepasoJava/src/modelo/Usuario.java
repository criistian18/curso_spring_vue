package modelo;
	//Diferencia entre una funcion y una constructor: que el constructor se llama igual que la clase
	// Tipoica clase Plain Old Java Object POJO: es una clase
public class Usuario extends Object {
	//crear 4 propiedades
	// MODELO: 
	//privatre solo peude acceder desde la clase
	//public puede acceder desde cualqiuer lugar
	//protected en las clases que heredan
	
	private int edad;
	private String nombre;
	
	
	public Usuario() { //DUDAAAS para ma�ana
		super();
		nombre = "Sin nombre";
	}
	//Sobre carga de cosntructores metodos
	//cionjunto de su nombre y sus parametros ( eso no debe coincidir )
	public Usuario(String nombre, int edad, String mascota) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		
	}
	public Usuario(String nombre, int edad) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		
	}
	public int getEdad() { //coger.
		return edad;
	}
	public void setEdad(int edad) { //poner.
		this.edad = edad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	// Con @ empieza las anotacione, que son extras al c�digo
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		Usuario usuario = (Usuario) obj;
		return this.nombre == usuario.nombre
				&& this.edad == usuario.edad;
	}
	public boolean equals(Usuario usuario) {//ejercidio equals a LOCO
		// TODO Auto-generated method stub
		return this.nombre == usuario.nombre
				&& this.edad == usuario.edad;
	}
	//SObreescribir: Machacar el metodo del padre
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Usuario " + nombre + "[" + edad + "]";
	}
	
	
	
}
