package com.grupoica.repasojava;

import java.util.HashMap;
import java.util.Scanner;

import modelo.Usuario;

public class EjemploHashMap {
	//K (string ) V valor(Usuario) 
	//Construir una clave compuesta
	/*public class ClaveDicc {
		String nombre;
		String fecha;
	}*/
	static HashMap<String, Usuario> diccUsuarios;	
	
	
	public static void probandoHashMap() {
		diccUsuarios = new HashMap<>();
		diccUsuarios.put("Luis", new Usuario("Luis",18));
		diccUsuarios.put("Ana", new Usuario("Ana",28));
		diccUsuarios.put("Pepe", new Usuario("Pepe",38));
		Scanner escaner = new Scanner(System.in);
		System.out.println("introduzco Usuario");
		String nombre = escaner.nextLine();
		System.out.println("EL usuaior es: " + diccUsuarios.get(nombre).toString());
		
	}

}
