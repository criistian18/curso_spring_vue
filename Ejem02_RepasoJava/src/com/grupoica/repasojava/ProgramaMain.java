package com.grupoica.repasojava;

import java.util.ArrayList;

import com.grupoica.repasojava.abst_interfaces.ProbandoVehiculo;

import modelo.Loco;
import modelo.Usuario;

public class ProgramaMain {
	
	/*P.O.O. (Programaci�n orientada a objetos)
	 * La unidad basica de almacenamientos son los tipos primitivos y los objetos
	 * que estan basado en clases. Las clases son el molde, plantilla, o estructura
	 *  que indica como se dan todos los objetos instaciados a partir de ella:
	 *  Sus variables miembro (campo, atributos, propiedades...) y sus metodos (funciones propias)
	 *  
	 *  -Herencia : La capacida delas clases para heredar los metdoso y variables miembro
	 *  unas de otras. Usando las palabras Extends
	 *  -Capsulaci�n: Capacidad de las clases para limitar el acceso a variable miembro y metodos
	 *  (Nivel de aceso private, public, protected ( private a nivel herencia ) o por defecto ( a nivel
	 *  de paquete)
	 *  -Poliformismo: La capacidad de los objetos de obtener su forma la forma de su clase o la de 
	 *  cualquiera de sus ancestros (padre, abuelo, etc..)
	 *  
	 *  DUDA PATRON DE DISE�O
	 */

	public static void main(String[] args) {
		//System.out.println("xx = " + EjemploMemoria.xx);
		//GestionUsuarios gesUsu = new GestionUsuarios();
		//Usuario u = new Usuario("pepe", 10);
		//EjemploLambdas.ejecutarLambdas();
		System.out.println();
		ProbandoVehiculo.probar();
		
		
		//this.listaUsuarios = new ArrayList();
		
		
		//EjemploMemoria.pruebaPasoPorReferencia();
		//EjemploMemoria.pruebaPasoPorValor();
		//EjemploMemoria.pruebaPasoPorValor();
	//	EjemploMemoria.otraFuncion();
		//double doble = 10.533214;
		//int entero = (int) Math.floor(doble);
		
	}

}
