package com.grupoica.repasojava;

import java.util.ArrayList;

import modelo.Loco;
import modelo.Usuario;

/* 
 * Clase que se encargara de las operaciones C.R.U.D
 * Create Read Update Delete (Op. Alta, Baja, modificacion y consulta)
 * */

public class GestionUsuarios {
// array (Agrupaci�n de datos) : Colecci�n de elementos ( tama�o fijo, del mismo tipo )
//arraylist : es dinamico no hace falta tama�o fijo
	
	//objeto en su forma general
	//private ArrayList listaUsuarios;
	
	//lista en su forma generica (todos los elementos son del mimo tipo o algun heredero)
	
	private ArrayList<Usuario> listaUsuarios;
	//

public GestionUsuarios() {
	
	
	//ARRAYLIST
	super();
	this.listaUsuarios = new ArrayList();
	//this.listaUsuarios.add(10);
	Usuario usu = new Usuario();
	usu.setEdad(30);
	usu.setNombre("Rojelio");
	System.out.println("Nombre:" + usu.getNombre() + " Edad: " + usu.getEdad());
	//Si listaUsuario es un arraylist sin tipo, es qye su tipo es object, cada uno de los elementos
	// es object. Por lo tanto por lo tanto hacemos astin implicito de usu a objetc gracias al polimorfismo
	// todas las clases heredan de object
	this.listaUsuarios.add(usu);
	
	//this.listaUsuarios.add("Texto");
	//this.listaUsuarios.add(new Object());
	this.listarUsuario();
		
	
	
	//Sobrecarga de constructores metodos
	Usuario u2 = new Usuario("pepe",50,"coquer");
	System.out.println("Edad U2: " + u2.getEdad() + " Nombre: " + u2.getNombre());
	
	if (u2.equals(usu)) {
		System.out.println("Son iguales");
		
	}else {
		System.out.println("Son distintas");
	}
	//herencia CLASS LOCO
	Loco joker = new Loco();
	joker.setTipoLocura(true);
	if (joker.isTipoLocura()) {
		System.out.println("Joker esta loco: " + joker.toString());
	} else {
		System.out.println("Joker NO esta loco: " + joker.toString());
	}
	this.listaUsuarios.add(joker);
	joker.setNombre("Joker");
	this.mostrarUsuario("Joker");
	this.modificarUsuario("Joker");
	this.modificarEdadNomber("Rojelio",30 );
	
	System.out.println("Joker: " + joker.toString()); //Override
	System.out.println("Joker: " + joker.getNombre());//hereda del padre (Usuario)
	System.out.println("Joker: " + joker.getEdad());//hereda del padre (Usuario)
}
	
	public void listarUsuario() {
		
		for (int i = 0; i < this.listaUsuarios.size(); i++) {
			
			System.out.println(this.listaUsuarios.get(i));
			
		
		}
	
		
}
	//PARA BUSCAR UN USUARIO CAMBIAMOS VOID POD EL OBJ(Usuario) public Usuario ObtenerUsuario(String nombre)despues de IF un return usu
	public void mostrarUsuario(String nombre) {
		
		for (Usuario usu : listaUsuarios) {
			if (usu.getNombre().equals(nombre)) {
				System.out.println("ENCONTRADO: " + usu.getNombre());
			}
		}
		
	}
	
	public void modificarUsuario(String nombre) {
		
		for (Usuario usu : listaUsuarios) {
			if (usu.getNombre().equals(nombre)) {
				usu.setNombre("pepe");
			
				
				System.out.println("se ha cambiado el nombre de  Rojelio por pepe");
			}
		}
		
	}
	
	public void modificarEdadNomber(String nombre, int edad) {
		for (Usuario usu : listaUsuarios) {
			if (usu.getNombre().equals(nombre)) {
				usu.setNombre("pepe");	
				//System.out.println("nombre: " + usu.getNombre());
				if(usu.getEdad() == (edad)) {
					usu.setEdad(900);
					System.out.println("El nombre y edad se ha modificado. Nombre: " + usu.getNombre() + " y la edad: " + usu.getEdad());
				}
			}
		}
		
	}
	
	
	
	
}
