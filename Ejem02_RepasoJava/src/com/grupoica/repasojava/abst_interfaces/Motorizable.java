package com.grupoica.repasojava.abst_interfaces;

public interface Motorizable {
		void encender();

		void desplazarse(float distancia);
}
