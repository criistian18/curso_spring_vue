package com.grupoica.repasojava.abst_interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;




public class ProbandoVehiculo {

	public static void probar() {
		Coche miCoche = new Coche("Kia", 1500, 60.34f);
		miCoche.aceleracion();
		Coche miCocheFines = new Coche("Hammer", 2500, 60.34f);
		miCocheFines.aceleracion();
		Caballo miCaballo = new Caballo("Unicornio", 450, 50);
		miCaballo.aceleracion();
		// Polimorfismo: pero lo que se pasa es la referencia 
		// (el puntero, la direccin de memoria)
		Vehiculo unVehiculo = miCoche;	// Casting implcito
		Object unObjeto = miCoche;
		Coche unCoche = (Coche) unObjeto;	// Casting explcito
		System.out.println(unObjeto.toString());
		unVehiculo.aceleracion();
		ArrayList<Motorizable> garaje = new ArrayList<>();
		// garaje.add(miCaballo); // Caballo no es motorizable
		garaje.add(miCoche);
		garaje.add(miCocheFines);
		// No se puede
		// garaje.add(new Vehiculo("Que no he comprado", 30));
		garaje.add((Coche) unVehiculo);
		garaje.add(new Patinete(15));
		
		for (Motorizable objMotor : garaje) {			
			objMotor.encender();
			if (objMotor instanceof Vehiculo ) {
				Vehiculo vehiculo = (Vehiculo) objMotor;
				vehiculo.aceleracion();
				vehiculo.desplazarse(1.5f);
			}
		}
		System.out.println("");
		Motorizable vehMotor = miCoche;
		vehMotor.encender();
		
		HashMap<String, Animial> granja = new HashMap<>();
		granja.put("Perro", new perro("Guau"));
		granja.put("caballo", new Caballo("Pura sangre", 200, 50));
		granja.put("caballo2", miCaballo);
		for (Map.Entry<String, Animial> animal : granja.entrySet()) {
			animal.getValue().alimentarse("Pollo");
			animal.getValue().alimentarse("calabacin");
		}
	
	}
	


	}


/*/Ejercicio 1- Garaje sera solo para objetos motorizables
//			2- Crear clase patiente que sea motorizable, pero no vehiculo
//			3-Guarderemos un patinete en el garaje 
//			4- Hacer una clase Perro (que tampoco es un veh�culo
//			5-5 - Crear una interfaz Animal con metodo Alimentarse
//			6 - Perro y Caballo que sean animales, y hacer una granja
		//y alimentarlos.*/



