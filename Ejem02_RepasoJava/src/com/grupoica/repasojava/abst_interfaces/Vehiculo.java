package com.grupoica.repasojava.abst_interfaces;

import java.util.ArrayList;

//si no queremos que sea posible instancia una clase,la declaramos abstracta
public  abstract class Vehiculo {
	
	protected String marca;
	protected float peso;
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public float getPeso() {
		return peso;
	}
	public void setPeso(float peso) {
		this.peso = peso;
	}
	public Vehiculo(String marca, float peso) {
		super();
		this.marca = marca;
		this.peso = peso;
	}
	
	public void aceleracion() {
		System.out.println(getClass().getSimpleName() + "marca"
		+ "Acelerando"		);
	}
	// interfaces con metodos abtractos
	//un metodo que lleva la palabra clave abstract ( el tipo de dato que devuelve
	//el noombre, los parametros pero no lleva implementacion)
	
	public abstract void desplazarse(float distancia);
	//protected abstract void desplazarse(float distancia);
	
	
}
