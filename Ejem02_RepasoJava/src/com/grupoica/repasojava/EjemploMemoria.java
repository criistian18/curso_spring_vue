package com.grupoica.repasojava;

import modelo.Usuario;

public class EjemploMemoria {
		static int xx = 10; //es unica y global
							
		public static void pruebaPasoPorValor() {
			//duda ma�ana
			
			boolean y = true; 
			String z = "Texto al declarar";
			funcionCualquiera(xx, y, z);
			System.out.println("XX = " + xx + ", Y = " + y + ", Z = " + z);
		}
		// Las variable primitivas ( y string) se pasan por valor
		// Se crean copias de las variables
		private static void funcionCualquiera(int x, boolean y, String z) {
			System.out.println("X = " + x + ", Y = " + y + ", Z = " + z);
			x = 200;
			y = false;
			z = "Texto nuevo dentro de funcion";
			System.out.println("X = " + x + ", Y = " + y + ", Z = " + z);
		}
		
		public static void pruebaPasoPorReferencia() {
			//Declaracion variable : reservamos un espacio peque�o solo para la direccion de memoria ( que tiene valor 0)
			// es decir como mucho entre 4 y 8 bytes
			// En realida declaramos una referencia a un objeto
			Usuario alguien;
			//Instaciacion del objeto
			//Instaciacion: es la reserva de memoria para todos los campos ( 4 + 8 = 12 byte)
			//llamada al constructor: se reservan otro 20 byte ( 12 + 20 = 32 bytes )
			//Asignaci�n coge la direccion de memoria que devuelve new y la pone 
			alguien = new Usuario ("Pepito el del sexto", 30);
			int array[] = new int[3];
			array[0] = 10; array[1] = 20; array[2] = 30;
			otraFuncion(alguien,array);
			System.out.println("nombre = " + alguien.getNombre() + ", Elemento = " + array[0]);		
			 
			//esta funcion solo recibe 8 bytes por cada pareametro con la referencia
		}//dudas esta funcion ma�ana
		private static void otraFuncion(Usuario parUsu, int[] parArr) {
			System.out.println("nombre = " + parUsu.getNombre() + ", Elemento = " + parArr[0]);
			
			parUsu.setNombre("Modif en funcion");
			parArr[0] = 9999;
			System.out.println("nombre= " + parUsu.getNombre()
			+ ", Elemento 0 = " + parArr[0]);
					
		}
}
