package grupoica.ejercicio;

import java.util.ArrayList;


public class GestionUsuarios {
	
	private ArrayList<Usuarios> listaUsuarios;
	
	public GestionUsuarios() {
		super();
		this.listaUsuarios = new ArrayList();
	
		
	}
	
	//Listar usuarios
	
	
	public void ListarUsuarios() {
		for (int i = 0; i < this.listaUsuarios.size(); i++) {
			System.out.println(this.listaUsuarios.get(i));			
		}
	}
	//Mostrar usuarios
	public void MostrarUsuarios(String nombre) {		
		for ( Usuarios usu : listaUsuarios) {
			
			if (usu.getNombre().equalsIgnoreCase(nombre)) {
				System.out.println("Encontrado: " + usu.getNombre());			
			}			
		}		
	}
	//Obtener Usuario
	public Usuarios ObtenerUsuario (String nombre) {
		for (Usuarios usu : listaUsuarios) {
			if (usu.getNombre().equalsIgnoreCase(nombre)) {
				return usu;
			}
		}
		System.out.println("Usuario no encontrado: " + nombre);	
		return null;
	}
	//Eliminamos
	public void eliminarUsuario(String nombre) {

		Usuarios usuBorrar = ObtenerUsuario(nombre);
		listaUsuarios.remove(usuBorrar);	
		
	}
	public void eliminarTodos() {
		listaUsuarios.clear();
	}

	//modificamos edad y nombre
	public void modificar(String nombre, String nuevoNombre) {
		Usuarios usuModif = ObtenerUsuario(nombre);
		if (usuModif != null) 
			usuModif.setNombre(nuevoNombre);
	}
	public void modificar(String nombre, int nuevaEdad) {
		Usuarios usuModif = ObtenerUsuario(nombre);
		if (usuModif != null) 
			usuModif.setEdad(nuevaEdad);
	}


	public void add(String nombre, String apellido, String email, int edad, int telefono) {
		Usuarios nuevoUsu = new Usuarios(nombre, apellido, email, edad, telefono);
	}	
	
		// TODO Auto-generated method stub
		
	}


