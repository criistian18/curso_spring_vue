package grupoica.ejercicio;

public class Usuarios {
	private int edad, telefono;
	private String nombre;
	private String apellidos;
	private String email;
	

	
	public Usuarios(String nombre, String apellidos, int edad) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.apellidos = apellidos;
		
		
	}
	
	//sobrecargo el constructor
	
	public Usuarios (String nombre, String apellidos, String email, int edad, int telefono) {
		this.nombre = nombre;
		this.edad = edad;
		this.apellidos = apellidos;
		this.email = email;
		this.telefono = telefono;
		
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
