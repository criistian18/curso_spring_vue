
public class Cuaderno extends Libro implements Paginas {
	
	String marca;
	
	
	public Cuaderno(String nombre, int ISBN) {
		super(nombre, ISBN);
		this.marca = marca;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	@Override
	public void muestra() {
		// TODO Auto-generated method stub
		System.out.println("El nombre es: " + nombre + " y el ISBN es: " + ISBN +  "del Cuaderno");
	}

	@Override
	public void Laspaginas(float paginas) {
		// TODO Auto-generated method stub
		System.out.println(" " + nombre + "tiene: " + paginas + " de paginas." );
	}

	@Override
	public void Laspaginas(int paginas) {
		// TODO Auto-generated method stub
		System.out.println("el cuaderno " + nombre + "tiene: " + paginas + " de paginas." );
	}
	
	public void Abrir() {
		System.out.println( "El cuaderno " + nombre + " esta abierto");
	}
	
	
	
}
