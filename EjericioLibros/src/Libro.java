
public abstract class Libro {
	
	protected String nombre;
	protected int ISBN;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getISBN() {
		return ISBN;
	}
	public void setISBN(int iSBN) {
		ISBN = iSBN;
	}
	
	public Libro(String nombre, int ISBN){
		super();
		this.nombre=nombre;
		this.ISBN=ISBN;
	}
	
	
	public void muestra() {
		System.out.println("El libro es: " + nombre + " con el ISBN: " + ISBN);
	}
	

	public abstract  void Laspaginas(int paginas);
	
	

}
