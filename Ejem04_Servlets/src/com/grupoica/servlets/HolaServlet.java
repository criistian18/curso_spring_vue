package com.grupoica.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HolaServlet
 */
@WebServlet("/hola")
public class HolaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HolaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String strNombre = request.getParameter("nombre");
		String html ="<html><head><title>Formulario de envio</title></head>" + 
				"<body>";
				if(strNombre == null || "".equals(strNombre)) {
					html += "<h2>POn el Nombrez</h2>";
				}else {
					html +="<form action=\"./hola\" method=\"post\">\r\n" + 
							" Veces: <input name = 'veces' type='number'/>" +
							"<input type=\"submit\" value=\"POST\">\r\n" + 
							"</form></body>" + "</html>";
				}
				
		// TODO Auto-generated method stub
		//response.getWriter().append("<h1>Hola desde servlet</h1> at: ").append(request.getContextPath());
		response.getWriter().append(html);
		
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String strVeces = request.getParameter("veces");
		String html ="<html><head><title>Formulario de envio</title></head>" + 
				"<body>";
				
		if(strVeces == null || "".equals(strVeces)) {
			html += "<h2>No has puesto datos</h2>";
		}else {
			int numero = Integer.parseInt(strVeces);
			for (int i = 0; i < numero; i++) {
				html += "<li>" + i + "</li> " + "</form></body>" + "</html>";
			}
		}
		response.getWriter().append("<h1>Hola desde servlet</h1> at: ").append(request.getContextPath());
		
		
		
		
	}


}
