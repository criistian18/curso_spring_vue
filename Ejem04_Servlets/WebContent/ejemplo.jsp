<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1> Ejemplo JSP: Es un servlet</h1>
<%! String crearOL ( int v ){
	String ol = "<OL>";
	int i = v;
	while (i > 0){
		ol += "<li>Cuenta Atras " + i + " /5 </li>";
		i--;
 	}
	return ol + "</ol>";
}
	%>
<hr>
<%-- --%>
<%
Date d = new Date();
out.println(d.toString());
if (d.getSeconds() % 2 == 0) { %>
	<p style ="background-color: red"> Prueba otra vez hasta un segundo impar</p>
	<%} else { %>
	<p style ="background-color: blue"></p>
	<%= crearOL(5) %>
	<%-- Este comentario es como es /**/ de hava no se envia al cliente --%>
	<!-- Pere este comentario si se envia -->
	<%-- Lo anterior <%= es lo mimoso que estoi: out.println( crearOL(5)); --%>
	<%} %>

</body>
</html>